#!/usr/bin/env python

import math
import random
import re
import sys

# RAM:
#     CPU registers
#     Memory
#
# Atomic ops:
#     x:              register address
#     c:              constant
#     l:              algorithm line label
#     s:              string
#     set x c:        set register x to c
#     set x1 x2:      set register x1 to x2
#     add x1 x2 x3:   store x1 + x2 in x3
#     sub x1 x2 x3:   store x1 - x2 in x3
#     mul x1 x2 x3:   store x1 * x2 in x3
#     div x1 x2 x3:   store x1 / x2 in x3
#     eq x1 x2 l:     if x1 == x2 go to line l
#     lte x1 x2 l:    if x1 <= x2 go to line l
#     lt x1 x2 l:     if x1 < x2 go to line l
#     gte x1 x2 l:    if x1 >= x2 go to line l
#     gt x1 x2 l:     if x1 > x2 go to line l
#     load x1 x2:     load memory x1 into register x2
#     save x1 x2:     set memory x1 to register x2
#     goto l:         go to line l
#     print s:        print string
#     printr x:       print register
#     printstate:     print memory and cpu
#     olist c1 c2:    put ordered list at memory c1 of length c2
#     rlist c1 c2:    put random list at memory c1 of length c2

CPU_SIZE = 24
MEM_SIZE = 20
TIMEOUT = 1000

arithmetic = {
    "add": lambda a, b: a + b,
    "sub": lambda a, b: a - b,
    "mul": lambda a, b: a * b,
    "div": lambda a, b: math.floor(a / b),
}

compare = {
    "eq": lambda a, b: a == b,
    "lte": lambda a, b: a <= b,
    "lt": lambda a, b: a < b,
    "gte": lambda a, b: a >= b,
    "gt": lambda a, b: a > b,
}

class RAM(object):

    def __init__(self):
        self.cpu = [0 for i in range(CPU_SIZE)]
        self.mem = [0 for i in range(MEM_SIZE)]

    def run(self, script):
        # Load script file
        with open(script) as f:
            print("parsing")
            lines = f.readlines()

            # Strip grammar
            lines = [line.strip() for line in lines]

            # Remove comments
            lines = [line for line in lines if len(line) > 0 and line[0] != "#"]

            # Parse lines
            commands = [self.parse(line) for line in lines]

            # Check for errors
            if None in commands:
                print("syntax error:", lines[commands.index(None)])
                return

            # Get line labels
            line_labels = {}
            for command_index in range(len(commands)):
                if commands[command_index][0] == "label":
                    line_labels[commands[command_index][1]] = command_index

            # Do things
            print("starting")
            timeout = TIMEOUT
            command_index = 0
            cost = 0
            while command_index < len(commands):
                command = commands[command_index]

                if command[0] == "set":
                    if len(command[2]) == 1 and ord(command[2]) >= ord("a"):
                        self.cpu_set(command[1], self.cpu_get(command[2]))
                    else:
                        self.cpu_set(command[1], int(command[2]))
                    cost += 1

                if command[0] in ["add", "sub", "mul", "div"]:
                    self.cpu_set(
                        command[3],
                        arithmetic[command[0]](
                            self.cpu_get(command[1]),
                            self.cpu_get(command[2]),
                        ))
                    cost += 1

                if command[0] in ["eq", "lte", "lt", "gte", "gt"]:
                    if compare[command[0]](
                            self.cpu_get(command[1]), self.cpu_get(command[2])):
                        command_index = line_labels[command[3]] - 1
                    cost += 1

                if command[0] == "load":
                    self.cpu_set(command[2],
                        self.mem_get(self.cpu_get(command[1])))

                if command[0] == "save":
                    self.mem_set(self.cpu_get(command[1]), self.cpu_get(command[2]))

                if command[0] == "goto":
                    command_index = line_labels[command[1]] - 1

                if command[0] == "print":
                    print(command[1])

                if command[0] == "printr":
                    print(command[1], "=", self.cpu_get(command[1]))

                if command[0] == "printstate":
                    print("CPU", self.cpu)
                    print("MEM", self.mem)

                if command[0] in ["olist", "rlist"]:
                    start = int(command[1])
                    length = int(command[2])
                    if command[0] == "olist":
                        self.mem[start:start+length] = sorted([
                            random.randint(1, 99) for r in range(length)])
                    else:
                        self.mem[start:start+length] = [
                            random.randint(1, 99) for r in range(length)]

                # Go to next line
                command_index += 1

                # decrement timeout
                timeout -= 1
                if timeout <= 0:
                    print("script timed out after", TIMEOUT, "lines")
                    break

            # Print state
            print("COST", cost)
            print("CPU", self.cpu)
            print("MEM", self.mem)

    def parse(self, line):
        # Line label
        m = re.search("^([a-z]+):$", line)
        if m:
            return ["label", m.group(1)]

        # Command
        m = re.search(
            "^(set|add|sub|mul|div|eq|lte|lt|gte|gt|load|save|goto|printstate|printr|print|olist|rlist) ?([-+]?[a-z0-9]+)? ?([-+]?[a-z0-9]+)? ?([-+]?[a-z0-9]+)?$",
            line)
        if m:
            return [group for group in m.groups() if group is not None]

        # No match
        return None

    def cpu_set(self, register, value):
        self.cpu[ord(register) - ord("a")] = value

    def cpu_get(self, register):
        return self.cpu[ord(register) - ord("a")]

    def mem_set(self, address, value):
        self.mem[address] = value

    def mem_get(self, address):
        return self.mem[address]

if __name__ == "__main__":
    ram = RAM()
    ram.run(sys.argv[1])
